﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyPrefab;

	void Start () {
		//spawn enemies, but underneath/within the EnemyFormation gameobject
		foreach (Transform child in transform) {
			GameObject enemy = Instantiate (enemyPrefab, child.transform.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
